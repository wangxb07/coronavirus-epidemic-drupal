<?php

namespace Drupal\location_flag_rest\Plugin\rest\resource;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Drupal\node\NodeStorage;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "location_flag_rest_resource",
 *   label = @Translation("Location flag rest resource"),
 *   uri_paths = {
 *     "canonical" = "/rest/location-flag/{id}",
 *     "create" = "/rest/location-flag"
 *   }
 * )
 */
class LocationFlagRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var NodeStorage
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('rest');
    $instance->currentUser = $container->get('current_user');
    /** @var EntityTypeManager $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');
    $instance->nodeStorage = $entityTypeManager->getStorage('node');
    $instance->serializerFormats = $container->getParameter('serializer.formats');
    return $instance;
  }

  /**
   * Responds to GET requests.
   *
   * @param string $id
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($id) {
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    /** @var Node $node */
    $node = $this->nodeStorage->load($id);

    if (!$node) {
      throw new HttpException(404, 'Not Found');
    }

    $location_flag = [
      'id' => $node->id(),
      'uuid' => $node->uuid(),
      'name' => $node->getTitle(),
    ];

    $location = $node->get('field_map_point');
    if ($location) {
      $location_flag['geo'] = $location[0];
    }

    return new ResourceResponse($location_flag, 200);
  }

  /**
   * Responds to location flag POST requests and saves the new location flag.
   *
   * @param array $data
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   */
  public function post($data) {
    if (empty($data['name']) || empty($data['lat']) || empty($data['lon'])) {
      throw new BadRequestHttpException('No entity content received.');
    }

    if (!$this->currentUser->hasPermission('create user_trace content')) {
      throw new AccessDeniedHttpException();
    }

    $node = Node::create([
      'title' => $data['name'],
      'type' => 'user_trace'
    ]);

    $point = [
      'lat' => $data['lat'],
      'lon' => $data['lon'],
    ];
    $value = \Drupal::service('geofield.wkt_generator')->WktBuildPoint($point);

    $node->field_map_point->setValue($value);

    try {
      $node->save();
      return new ModifiedResourceResponse($node, 201);
    }
    catch (EntityStorageException $e) {
      throw new HttpException(500, 'Internal Server Error', $e);
    }
  }
}
