<?php

namespace Drupal\wechat\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class WechatMerchantForm.
 */
class WechatMerchantForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $wechat_merchant = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $wechat_merchant->label(),
      '#description' => $this->t("Label for the Wechat merchant."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $wechat_merchant->id(),
      '#machine_name' => [
        'exists' => '\Drupal\wechat\Entity\WechatMerchant::load',
      ],
      '#disabled' => !$wechat_merchant->isNew(),
    ];

    $form['mini_program_app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('mini_program_app_id'),
      '#default_value' => $wechat_merchant->getMiniProgramAppId(),
    ];

    $form['mini_program_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('mini_program_secret'),
      '#default_value' => $wechat_merchant->getMiniProgramSecret(),
    ];

    $form['official_account_app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('official_account_app_id'),
      '#default_value' => $wechat_merchant->getOfficialAccountAppId(),
    ];

    $form['official_account_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('official_account_secret'),
      '#default_value' => $wechat_merchant->getOfficialAccountSecret(),
    ];
    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $wechat_merchant = $this->entity;
    $status = $wechat_merchant->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Wechat merchant.', [
          '%label' => $wechat_merchant->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Wechat merchant.', [
          '%label' => $wechat_merchant->label(),
        ]));
    }
    $form_state->setRedirectUrl($wechat_merchant->toUrl('collection'));
  }

}